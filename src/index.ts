export {
    Cacheable,
    globalInit,
    localStorageInit,
    globalClear,
    globalDelete,
    globalGet,
    globalSet,
    localStorageClear,
    localStorageDelete,
    localStorageGet,
    localStorageSet,
    globalMethods,
    globalKeys,
    localStorageMethods,
    localStorageKeys,
} from './Cacheable'
export { CacheableOptions } from './CacheableOptions'
export { Scope } from './Scope'
