/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable unused-imports/no-unused-vars */
import { ExpiringMap } from '../ExpiringMap'
import { CacheableOptions, optionsWithDefaults } from '../CacheableOptions'
import { CacheRegistry, Cache, MethodMap } from './CacheRegistry'

export class LocalStorageCacheRegistry extends CacheRegistry {
    private getStoreFunction: () => unknown | undefined = undefined

    public getOrInitCache(target: unknown, methodName?: string): Cache {
        const cache = super.getOrInitCache(target, methodName)
        const targetClass = target.constructor.name
        if (!cache.has(targetClass)) {
            cache.set(targetClass, new Map<string, unknown>())
        }
        return <Cache>cache.get(targetClass)
    }

    protected getCacheHost(target: unknown, methodName?: string): unknown {
        if (this.getStoreFunction === undefined) {
            throw `getStore function is undefined for ${this.getTarget(target, methodName)}`
        }
        const host = this.getStoreFunction()
        if (host === undefined) {
            throw `getStore function returned undefined for ${this.getTarget(target, methodName)}`
        }
        return host
    }

    protected newCache(): Map<string, MethodMap> {
        return new Map<string, Map<string, ExpiringMap<string, unknown>>>()
    }

    protected processOptions(target: unknown, methodName: string, options?: CacheableOptions): void {
        if (this.getStoreFunction !== undefined) {
            return
        }
        if (options === undefined || options.getStore === undefined) {
            throw `options.getStore is required for ${this.getTarget(target, methodName)}. options: ${JSON.stringify(options)}`
        }
        this.getStoreFunction = options.getStore
    }

    public initCache(cacheHost?: unknown): void {
        if (cacheHost === undefined) {
            super.initCache(this.getCacheHost(undefined))
        } else {
            super.initCache(cacheHost)
        }
    }

    public initialiseStore(storeFunction: () => unknown): void {
        this.processOptions(undefined, '?', optionsWithDefaults({ scope: 'LOCAL_STORAGE', getStore: storeFunction }))
        this.initCache()
    }

    private getTarget(target: unknown, methodName: string): string {
        return (target === undefined ? '?' : target.constructor.name) + '::' + (methodName ?? '?')
    }
}
