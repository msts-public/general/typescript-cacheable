import { ExpiringMap } from '../ExpiringMap'
import { MethodMap, CacheRegistry } from './CacheRegistry'

export class GlobalCacheRegistry extends CacheRegistry {
    protected newCache(): MethodMap {
        return new Map<string, ExpiringMap<string, unknown>>()
    }
}
