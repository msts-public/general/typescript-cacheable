/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable unused-imports/no-unused-vars */
import { faker } from '@faker-js/faker'
import { Cacheable } from '../src/Cacheable'
import { Dwarf } from './Dwarf'
import { getStore } from './SimpleApp'

export class DwarfRepository {
    private isGrumpy = false

    @Cacheable()
    public nonAsync(): number {
        // eslint-disable-next-line no-empty
        for (let i = 0; i < 1000000000; i++) {}
        return 1000000000
    }

    @Cacheable()
    public async findHappiest(): Promise<Dwarf> {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(new Dwarf('Huck', 'Finn'))
            }, 100)
        })
    }

    @Cacheable()
    public async findSaddest(): Promise<Dwarf | undefined> {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(undefined)
            }, 100)
        })
    }

    @Cacheable({ cacheUndefined: true })
    public async findGrumpiest(): Promise<Dwarf | null> {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(null)
            }, 100)
        })
    }

    @Cacheable({ cacheUndefined: false })
    public async findGrumpiestWithoutCachingNulls(): Promise<Dwarf | null> {
        return new Promise((resolve) => {
            setTimeout(() => {
                if (this.isGrumpy) {
                    resolve(new Dwarf(`Mark`, `MyWords`))
                } else {
                    this.isGrumpy = true
                    resolve(null)
                }
            }, 100)
        })
    }

    @Cacheable({ ttl: 1000 })
    public async findHappiestWithTimeout(): Promise<Dwarf> {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(new Dwarf('Huck', 'Finn'))
            }, 100)
        })
    }

    @Cacheable({ scope: 'LOCAL_STORAGE', getStore: getStore })
    public async findRandom(): Promise<Dwarf> {
        return new Promise((resolve) => {
            setTimeout(() => {
                const dwarf = new Dwarf(faker.name.firstName(), faker.name.lastName())
                resolve(dwarf)
            }, 100)
        })
    }

    @Cacheable({ scope: 'LOCAL_STORAGE', getStore: getStore })
    public async findTheAnswer(): Promise<number> {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(42)
            }, 100)
        })
    }

    @Cacheable({ scope: 'LOCAL_STORAGE', getStore: getStore })
    public async findUncle(name: string): Promise<Dwarf> {
        return new Promise((resolve) => {
            setTimeout(() => {
                const dwarf = new Dwarf(faker.name.firstName(), faker.name.lastName())
                resolve(dwarf)
            }, 100)
        })
    }

    @Cacheable({ scope: 'LOCAL_STORAGE', getStore: getStore })
    public async findAuntie(p1: string, p2: boolean): Promise<Dwarf> {
        return new Promise((resolve) => {
            setTimeout(() => {
                const dwarf = new Dwarf('Jane', 'Doe')
                resolve(dwarf)
            }, 100)
        })
    }

    @Cacheable()
    public async countByLastName(name: string): Promise<number> {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(12)
            }, 100)
        })
    }

    @Cacheable()
    public async countByFirstAndLastName(firstName: string | null, lastName: string | undefined): Promise<number> {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(1)
            }, 50)
        })
    }

    @Cacheable()
    public async findWithInterestsMatching(customer: Dwarf): Promise<Dwarf> {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(new Dwarf('Huck', 'Finn'))
            }, 100)
        })
    }

    @Cacheable()
    public greetDwarf(name: string | undefined | null): string {
        return `Hello, ${name || 'dwarf'}!`
    }
}
