/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { AsyncLocalStorage } from 'async_hooks'
import express from 'express'
import Stopwatch from 'statman-stopwatch'
import { ExpiringMap } from '../src/ExpiringMap'
import { cacheProperty, ClassMap } from '../src/registry/CacheRegistry'
import { localStorageClear, localStorageDelete, localStorageSet, localStorageGet, localStorageInit } from '../src/Cacheable'
import { DwarfRepository } from './DwarfRepository'
import { Dwarf } from './Dwarf'

export const app = express()
const als = new AsyncLocalStorage<Context>()

export const getStore = (): unknown => {
    return als.getStore()
}

class Context {}

function localObjectCache(target: unknown, method: string): ExpiringMap<string, any> {
    const context = als.getStore()
    expect(context).toBeDefined()
    const classMap = <ClassMap>context[cacheProperty]
    const methodMap = classMap.get(target.constructor.name)
    if (methodMap !== undefined) {
        return methodMap.get(method)
    }
    return undefined
}

app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    const context = new Context()
    als.run(context, () => {
        return next()
    })
})

app.get('/hello', async (req: express.Request, res: express.Response): Promise<void> => {
    const dwarfRepo = new DwarfRepository()
    const watch = new Stopwatch()
    watch.start()
    const dwarf = await dwarfRepo.findRandom()
    let time = watch.read()
    expect(time).toBeGreaterThanOrEqual(99)
    watch.reset()
    watch.start()
    const anotherDwarf = await dwarfRepo.findRandom()
    expect(`${dwarf.firstName} ${dwarf.lastName}`).toEqual(`${anotherDwarf.firstName} ${anotherDwarf.lastName}`)
    time = watch.read()
    expect(time).toBeLessThan(10)
    const theAnswer = await dwarfRepo.findTheAnswer()
    expect(theAnswer).not.toEqual(dwarf)
    res.send({ dwarf: anotherDwarf })
})

app.get('/inittest', async (req: express.Request, res: express.Response): Promise<void> => {
    const dwarfRepo = new DwarfRepository()
    localStorageInit(getStore)
    const names: string[] = ['doofus', 'dilroy']
    await dwarfRepo.findRandom()
    for (let i = 0; i < 2; i++) {
        for (const name of names) {
            await dwarfRepo.findUncle(name)
        }
    }
    localStorageInit(getStore)
    for (const method of ['findRandom', 'findUncle']) {
        const cache = localObjectCache(dwarfRepo, method)
        expect(cache).toBeUndefined()
    }
    res.send({ ok: true })
})

app.get('/cleartest', async (req: express.Request, res: express.Response): Promise<void> => {
    const dwarfRepo = new DwarfRepository()
    localStorageInit(getStore)
    localStorageClear(dwarfRepo, 'findRandom')
    const names: string[] = ['doofus', 'dilroy']
    await dwarfRepo.findRandom()
    for (let i = 0; i < 2; i++) {
        for (const name of names) {
            await dwarfRepo.findUncle(name)
        }
    }
    for (const method of ['findRandom', 'findUncle']) {
        const cache = localObjectCache(dwarfRepo, method)
        expect(cache.size).toBeGreaterThan(0)
        localStorageClear(dwarfRepo, method)
        expect(cache.size).toEqual(0)
    }
    res.send({ ok: true })
})

app.get('/deletetest', async (req: express.Request, res: express.Response): Promise<void> => {
    const dwarfRepo = new DwarfRepository()
    const names: string[] = ['doofus', 'dilroy']
    await dwarfRepo.findRandom()
    for (let i = 0; i < 2; i++) {
        for (const name of names) {
            await dwarfRepo.findUncle(name)
        }
    }
    let cache: ExpiringMap<string, any>
    cache = localObjectCache(dwarfRepo, 'findRandom')
    expect(cache.size).toEqual(1)
    localStorageDelete(dwarfRepo, 'findRandom', [])
    expect(cache.size).toEqual(0)
    cache = localObjectCache(dwarfRepo, 'findUncle')
    let numKeys = names.length
    for (const name of names) {
        expect(cache.size).toEqual(numKeys)
        localStorageDelete(dwarfRepo, 'findUncle', [name])
        --numKeys
        expect(cache.size).toEqual(numKeys)
    }
    res.send({ ok: true })
})

app.get('/settest', async (req: express.Request, res: express.Response): Promise<void> => {
    const dwarfRepo = new DwarfRepository()
    const newDwarf = new Dwarf('horrendo', 'revolver')
    const updateName = 'dilroy'
    const names: string[] = ['doofus', updateName]
    const original = new Map<string, any>()
    const dwarfRandom = await dwarfRepo.findRandom()
    localStorageSet(dwarfRepo, 'findRandom', [], newDwarf)
    const dwarfRandom2 = await dwarfRepo.findRandom()
    expect(dwarfRandom2).not.toEqual(dwarfRandom)
    expect(dwarfRandom2).toEqual(newDwarf)

    for (const name of names) {
        original.set(name, await dwarfRepo.findUncle(name))
    }
    localStorageSet(dwarfRepo, 'findUncle', [updateName], newDwarf)
    for (const name of names) {
        const cacheVal = await dwarfRepo.findUncle(name)
        if (name === updateName) {
            expect(cacheVal).toEqual(newDwarf)
        } else {
            expect(cacheVal).toEqual(original.get(name))
        }
    }
    await dwarfRepo.findAuntie('a', true)
    const harrietDoe = new Dwarf('Harriet', 'Doe')
    localStorageSet(dwarfRepo, 'findAuntie', ['a', false], harrietDoe)
    const unknown = await dwarfRepo.findAuntie('a', false)
    expect(unknown).toEqual(harrietDoe)
    res.send({ ok: true })
})

app.get('/gettest', async (req: express.Request, res: express.Response): Promise<void> => {
    const dwarfRepo = new DwarfRepository()
    const dwarfRandom = await dwarfRepo.findRandom()
    const dwarfRandomDirect = localStorageGet(dwarfRepo, 'findRandom', [])
    expect(dwarfRandom).toEqual(dwarfRandomDirect)
    const name = 'doofus'
    const dwarfUncle = await dwarfRepo.findUncle(name)
    const dwarfUncleDirect = localStorageGet(dwarfRepo, 'findUncle', [name])
    expect(dwarfUncle).toEqual(dwarfUncleDirect)
    res.send({ ok: true })
})
