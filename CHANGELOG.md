# 3.0.0

With this release we decided to 'bite the bullet' and use the native `AsyncLocalStorage` api and leave `cls-hooked` behind. This was
not a decision we took lightly since it necessitated a breaking change, but we had issues with `cls-hooked` storage not being
initialised at the start of a request, and the library appears to not be maintained any more.

So, to be clear, version 3 is not compatible with version 2.x if you are using `scope: 'LOCAL_STORAGE'`.

When we converted our code to use `AsyncLocalStorage` (ALS) instead of `cls-hooked` (CH), the main consideration was that with ALS you
always start with a fresh store whereas with CH if you start a new context from an existing context, the new context will start with
a copy of the 'parent' context.

Other than working through the above (which was mainly in our test suite), the transition was pretty straight forward.
